<?php
// login.php
//require_once ('../connection/bddconnection.php');
session_start();
if( isset($_POST['btn'])){
// Configura los datos de tu cuenta
    $dbhost='mysql-toysandshare.alwaysdata.net';  ## El servidor
    $dbusername='257522_toys'; ## Usuario
    $dbuserpass='toysandshare1'; ## Aqui va la clave de tu servidor de base de datos.
    $dbname='toysandshare_db';  ## Aqui va el nombre de la base de datos que te especifique arriba.

// Conectar a la base de datos
    $conn = mysqli_connect ($dbhost, $dbusername, $dbuserpass, $dbname);
//    mysqli_select_db($dbname) or die('Cannot select database');

## Escapamos los datos recibidos.
    $email = htmlentities(trim($_POST['email']));
    $password = htmlentities(trim($_POST['password'])); ## Es preferible que guarde tus password encriptados.

    ##Verificamos que el usuario contenga datos.
    if (isset($email) && empty($email) ) {
        echo "El usuario no fue instroducido";
        exit();
    }

    ## Verificamos que el password (HASH) contiene password (HASH).
    //poner hash en vez de password
    if ( isset($password) && empty($password) ) {
        echo "El password no fue instroducido";
        exit();
    }

    $select = "SELECT id,email,password FROM users WHERE email = '$email'";

    ## Hacemos la consulta de verificacion.
    $query = mysqli_query($conn,$select) or die(mysqli_error());

    ## Verificamos si regreso registro la consulta.
    if(mysqli_num_rows($query)){
        ## recuperamos los datos.
        $data = mysqli_fetch_array($query);
        if (password_verify($password, $data['password'])){
            ## Guadamos el usuario en la seccion.
            $_SESSION['id'] = $data['id'];
            $_SESSION['email'] = $data['email'];
            $_SESSION['password'] = $data['password'];
            ## Mostramos el mensaje.
            header("Location: /index.php");
            echo "Has sido logueado correctamente ".$_SESSION['s_id']." y puedes acceder al index.php.";
            exit();
        } else{
            ##Está intentando entrar sin permisos.
            echo "Login incorrecto";
        }


    } else {
        ##Está intentando entrar sin permisos.
        echo "Login incorrecto";

    }
}

?>


<style>
    @import url('https://fonts.googleapis.com/css2?family=Varela+Round&display=swap');
    /*@use postcss-preset-env {*/
    /*    stage: 0;*/
    /*}*/

    /* ---------- GENERAL ---------- */
    * {
        box-sizing: inherit;
        font-family: 'Varela Round', sans-serif;
    }

    html {
        box-sizing: border-box;
    }

    body {
        background-color: #7ED9C8;
        line-height: 1.5;
        margin: 0;
        min-block-size: 100vh;
        padding: 5vmin;
    }

    h2 {
        font-size: 1.75rem;
    }

    input {
        background-image: none;
        border: none;
        font: inherit;
        margin: 0;
        padding: 0;
        transition: all 0.3s;
    }

    svg {
        height: auto;
        max-width: 100%;
        vertical-align: middle;
    }

    /* ---------- ALIGN ---------- */
    .align {
        display: grid;
        place-items: center;
    }

    /* ---------- BUTTON ---------- */

    .button {
        background-color: #04BF9D;
        color: #fff;
        padding: 0.25em 1.5em;
    }

    .button:focus,
    .button:hover {
        background-color: #04BF9D;
    }

    /* ---------- ICONS ---------- */
    .icons {
        display: none;
    }

    .icon {
        fill: currentcolor;
        display: inline-block;
        height: 1em;
        width: 1em;
    }

    /* ---------- LOGIN ---------- */
    .login {
        width: 500px;
    }

    .login__header {
        background-color: #F23545;
        border-top-left-radius: 1.25em;
        border-top-right-radius: 1.25em;
        color: #055159;
        padding: 1.25em 1.625em;
    }

    .login__header :first-child {
        margin-top: 0;
    }

    .login__header :last-child {
        margin-bottom: 0;
    }

    .login h2 .icon {
        margin-right: 14px;
    }

    .login__form {
        background-color: #F2B705;
        border-bottom-left-radius: 1.25em;
        border-bottom-right-radius: 1.25em;
        color: #777;
        display: grid;
        gap: 0.875em;
        padding: 1.25em 1.625em;
    }

    .login input {
        border-radius: 0.1875em;
    }

    .login input[type="email"],
    .login input[type="password"] {
        background-color: #eee;
        color: #055159;
        padding: 0.25em 0.625em;
        width: 100%;
    }

    .login input[type="submit"] {
        display: block;
        margin: 0 auto;
    }

    .container:hover{
        -webkit-animation: tiembla 0.35s;

    }
    @-webkit-keyframes tiembla{
        0%  { -webkit-transform:rotateZ(-2deg); }
        50% { -webkit-transform:rotateZ( 0deg) scale(.8); }
        100%{ -webkit-transform:rotateZ( 2deg); }
    }

    /***/
    /** {*/
    /*    padding: 0;*/
    /*    box-sizing: border-box;*/
    /*}*/

    /*body {*/
    /*    margin: 50px auto;*/
    /*    text-align: center;*/
    /*    width: 800px;*/
    /*}*/

    /*h1 {*/
    /*    font-family: 'Passion One', serif;*/
    /*    font-size: 2rem;*/
    /*    text-transform: uppercase;*/
    /*}*/

    /*label {*/
    /*    width: 150px;*/
    /*    font-family: 'Passion One ', serif;*/
    /*    display: inline-flex;*/
    /*    text-align: left;*/
    /*    font-size: 1.5rem;*/
    /*}*/

    /*input {*/
    /*    border-radius: 0.5em;*/
    /*    border: 2px solid #000000;*/
    /*    font-size: 1.5rem;*/
    /*    font-weight: 100;*/
    /*    font-family: 'Lato', serif;*/
    /*    padding: 10px;*/
    /*}*/

    /*form {*/
    /*    margin: 25px auto;*/
    /*    padding: 20px;*/
    /*    border: 5px solid #000000;*/
    /*    width: 500px;*/
    /*    background: #7ed9c8;*/
    /*}*/

    /*div.form-element {*/
    /*    margin: 20px 0;*/
    /*}*/

    /*button {*/
    /*    padding: 10px;*/
    /*    font-size: 2vw;*/
    /*    font-family: 'Passion One', serif;*/
    /*    font-weight: 100;*/
    /*    background: #f23545;*/
    /*    color: #000000;*/
    /*    border: 2px solid #000000;*/
    /*}*/

    /*p.success,*/
    /*p.error {*/
    /*    color: white;*/
    /*    font-family: lato, "Lato Black", serif;*/

    /*    background: #055159;*/
    /*    display: inline-block;*/
    /*    padding: 2px 10px;*/
    /*}*/

    /*p.error {*/
    /*    background: orangered;*/
    /*}*/

</style>


<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="//maxcdn.bootstrap.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
          crossorigin="anonymous">
    <title>Login page</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="align">
<!--<h1>Log in</h1>-->
<?php //if ($submitted && !$authenticated): ?>
<!--    <div class="alert">-->
<!--        Invalid credentials.-->
<!--    </div>-->
<?php //endif; ?>
<?php //if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
//    //Valid email!
//
//} ?>
<div class="login" style="font-family: 'Times New Roman', Times, serif;">
    <div style="text-align: center">
        <h2>Build a better World together</h2>
        <p>Sharing & Caring isn’t that difficult</p>
    </div>
    <div class="container">
        <header class="login__header">
            <h2><svg class="icon">
                    <use xlink:href="#icon-lock" />
                </svg>Log in</h2>
        </header>
        <form method="POST" id="acceso" action="./login.php" class="login__form" name="signin-form">
            <div class="form-element">
                <label>Email</label>
                <input id="email" type="email" name="email" placeholder="mail@address.com" required />
            </div>
            <div class="form-element">
                <label>Password</label>
                <input id="password" type="password" class="acceso" name="password" placeholder="password" required />
            </div>
            <button class="button" type="submit" name="login-google" value="login-google">Log in with Google</button>
            <button class="button" type="submit" name="btn" value="login" >Log In</button>
        </form>
    </div>
</div>
<svg xmlns="http://www.w3.org/2000/svg" class="icons">

    <symbol id="icon-lock" viewBox="0 0 448 512">
        <path d="M400 224h-24v-72C376 68.2 307.8 0 224 0S72 68.2 72 152v72H48c-26.5 0-48 21.5-48 48v192c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V272c0-26.5-21.5-48-48-48zm-104 0H152v-72c0-39.7 32.3-72 72-72s72 32.3 72 72v72z" />
    </symbol>

</svg>
</body>
</html>
