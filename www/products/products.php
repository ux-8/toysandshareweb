<?php require_once ("../connection/bddconnection.php") ?>
<!doctype html>
<html lang="en">
<head>
    <title>Products</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <?php require_once ("../connection/bddconnection.php") ?>
</head>
<body>
<header>
</header>
<main>
    <a href="http://toysandshare.alwaysdata.net/products/products.php">page</a>
    <div class="products__content">

        <?php
        $sql = "SELECT * FROM products;";
        $result = mysqli_query($conn, $sql);
        $resultCheck = mysqli_num_rows($result);

        if ($result):
            if (mysqli_num_rows($result)>0):
                while ($product = mysqli_fetch_assoc($result)):
                    ?>

                    <div class="content__product_block">
                        <a href="http://toysandshare.alwaysdata.net/products/details.php?product=<?php echo $product['id']?>" class="product_link">
                            <img src="<?php echo $product['imageLink']?>" alt="horizontalCar" class="product_block__image">
                        </a>
                        <div class="product_block__text">
                                <h1 class="text_product__product_name"><?php echo $product['productName']?></h1>
                                <!--                distance or address?-->
                                <p class="text_product__distance"><?php echo $product['productLocation']?></p>

                                <?php

                                $donatorSQL = "SELECT * FROM users WHERE id='$product[donatorID]'";
                                $donatorInfo = mysqli_query($conn, $donatorSQL);
                                $donatorResult = mysqli_fetch_assoc($donatorInfo)

                                ?>
                                <p class="text_product__donator"><?php echo $donatorResult['name']?></p>
                                <p class="text_product__description"><?php

                                    $str=wordwrap(ucfirst(strtolower($product['productDescription'])),38,"<br>\n", true);
                                    preg_match_all("/\.\s*\w/", $str, $matches);

                                    foreach($matches[0] as $match){
                                        $str = str_replace($match, strtoupper($match), $str);
                                    }
                                    echo $str?></p>
                            </div>
                    </div>

                <?php
                endwhile;
            endif;
        endif;
        ?>
    </div>
</main>
</body>
</html>
