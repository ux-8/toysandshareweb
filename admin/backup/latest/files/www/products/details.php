<!doctype html>
<html lang="en">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport"
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://toysandshare.alwaysdata.net/css/main.css" type="text/css" media="all" />
    <link rel="icon" type="image/png" href="https://static.alwaysdata.com/aldjango/img/favicon.png" />
    <?php require_once ("../connection/bddconnection.php") ?>
</head>
<body>
<header>

</header>
<main>
    <a href="http://toysandshare.alwaysdata.net/products/details.php">page</a>
    <div class="details_main">
        <div class="info_top">
            <div class="product_images_slider">
                <!--        foreach image make function X-->
                <!--        https://www.w3schools.com/howto/howto_js_slideshow.asp -->
                <div class="slider__object fade">
                    <div class="slider__counter">1 / 3</div>
                    <img src="" class="slider__image"><!-- style="width:100%"-->
                    <div class="slider__text">Caption Text</div>
                </div>
                <!--
                            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                            <a class="next" onclick="plusSlides(1)">&#10095;</a>

                            <div style="text-align:center">
                                <span class="dot" onclick="currentSlide(1)"></span>
                                <span class="dot" onclick="currentSlide(2)"></span>
                                <span class="dot" onclick="currentSlide(3)"></span>
                            </div>-->
            </div>
            <div class="product_right">
                <div class="product_right__top">
                    <h1>Toy Car
                        “Hot Wheels”</h1>
                    <div class="product_price prod_info_line">
                        <p class="price_header info_line_header">Price:</p>
                        <p class="price_value info_line_value">0$</p>
                    </div>
                    <div class="product_donator prod_info_line">
                        <p class="donator_header info_line_header">Donator:</p>
                        <p class="donator_value info_line_value">Kendrick lamar</p>
                    </div>
                    <div class="product_address prod_info_line">
                        <p class="address_header info_line_header">Address:</p>
                        <p class="address_value info_line_value">N.Y. Jackobs 2-2/18</p>
                    </div>
                </div>
                <button class="product_dm">Contact the donator</button>
            </div>
        </div>
        <p class="product_description">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis posuere consequat.
            Donec maximus odio in venenatis sollicitudin.
            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
            Nunc quis sapien nulla. Donec elementum at est eget tincidunt.
            Suspendisse sed pretium nulla, nec sollicitudin magna.
            Fusce semper magna sit amet malesuada bibendum.
            Suspendisse at pretium nulla. Vestibulum mollis nunc in lorem tincidunt mattis.
            Aliquam diam ligula, sodales non lacus a, dictum pulvinar ipsum.
            Phasellus lacus lorem, aliquam a efficitur vel, scelerisque nec velit.
            Suspendisse diam mauris, molestie nec diam nec, pellentesque bibendum nulla.
            Maecenas cursus maximus enim non interdum. Nulla congue quam dictum, efficitur odio id, rutrum nisl.
            Quisque bibendum ex sit amet odio posuere pellentesque quis auctor lectus. Nam sed condimentum est.
        </p>
    </div>
</main>
</body>
</html>