<?php
session_start();
require_once ("../connection/bddconnection.php");
// check to see if the user successfully created an account




?>
<html>
<head>
    <title>php member system | registration form</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<script>
    function guardar(){
        let date = new Date();

        var user = jQuery("#username").val();
        var email = jQuery("#email").val();
        var password = jQuery("#password").val();
        var confirmerdPass = jQuery("#confirmPass").val()
        var fecha = date.getDate() + '-' + ( date.getMonth() + 1 ) + '-' + date.getFullYear();

        if (user !== '' && password !== '' && confirmerdPass !== '' ){
            if (password === confirmerdPass){
                if (password.length >= 5 && strp(password, "!#$.,:;()") !== false){

                }else{
                    alert("Tu contraseña no es lo suficientemente fuerte. Por favor utiliza otra")
                }
            }else{
                alert("Las contraseñas no coinciden.")
            }
        }else {
            alert("Por favor introduce los campos obligatorios.")
        }



    }
</script>

<body>
<h1>member system tutorial - register</h1>
<a href="./index.php">Home</a> |
<a href="./register.php">Register</a> |
<a href="./login.php">Login</a>
<hr />

<!-- error message code here -->

<!-- registration form html code here -->

</body>
</html>


<form action="./register.php" class="form" method="POST">

    <h1>Crear nueva cuenta</h1>

    <div class="">
        <?php
        // check to see if the user successfully created an account
        if (isset($success) && $success == true){
            echo '<p color="green">Yay!! Your account has been created. <a href="./login.php">Click here</a> to login!<p>';
        }
        // check to see if the error message is set, if so display it
        else if (isset($error_msg))
            echo '<p color="red">'.$error_msg.'</p>';

        ?>
    </div>

    <div class="">
        <input type="text" id="username" name="username" value="" placeholder="Usuario" autocomplete="off" required />
    </div>
    <div class="">
        <input type="text" id="email" name="email" value="" placeholder="Correo" autocomplete="off" required />
    </div>
    <div class="">
        <input type="password" id="password" name="passwd" value="" placeholder="Contraseña" autocomplete="off" required />
    </div>
    <div class="">
        <p>password must be at least 5 characters and<br /> have a special character, e.g. !#$.,:;()</font></p>
    </div>
    <div class="">
        <input type="password" id="confirmPass" name="confirm_password" value="" placeholder="confirm your password" autocomplete="off" required />
    </div>

    <div class="">
        <input class="" type="submit" name="registerBtn" value="create account"  onclick="guardar()"/>
    </div>

    <p class="center"><br />
        Already have an account? <a href="<?php echo SITE_ADDR ?>/login">Login here</a>
    </p>
</form>

<?php
if (isset($success) && $success == true){
    echo '<font color="green">Yay!! Your account has been created. <a href="./login.php">Click here</a> to login!<font>';
}
// check to see if the error message is set, if so display it
else if (isset($error_msg))
    echo '<font color="red">'.$error_msg.'</font>';
else
    echo ''; // do nothing

?>
