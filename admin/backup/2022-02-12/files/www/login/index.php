<?php
// index.php
session_start();

// If user is logged in, retrieve identity from session.
//$usuario = null;
//if (isset($_SESSION['usuario'])) {
//    $usuario = $_SESSION['usuario'];
//}

//Evitamos que nos salgan los NOTICES de PHP
error_reporting(E_ALL ^ E_NOTICE);

//Comprobamos si la sesión está iniciada
//Si existe una sesión correcta, mostramos la página para los usuarios
//Sino, mostramos la página de acceso y registro de usuarios
if(isset($_SESSION['usuario']) and $_SESSION['estado'] == 'Autenticado') {
    include('index.php');
    die();
} else {
    include('login.php');
    die();
};
?>

<!DOCTYPE html>
<html>
<head>
    <title>Home page</title>
</head>
<body>
<h1>Home</h1>
<?php if ($usuario==null): ?>
    <a href="login.php">Sign in</a>
<?php else: ?>
    <strong>Welcome, <?= $usuario ?></strong> <a href="logout.php">Sign out</a>
<?php endif; ?>

<p>
    This is a simple website to demonstrate the advantages of a PHP framework
    and disadvantages of "pure" PHP.
</p>
</body>
</html>