<?php
require_once ("../connection/bddconnection.php");
session_start();
//// check to see if the user successfully created an account
//if (isset($_SESSION['username']) && isset($_SESSION['userid']))
//    header("Location: ./index.php");
//
//

?>
<html>
<head>
    <title>php member system | registration form</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://static.alwaysdata.com/css/administration.css" type="text/css" media="all" />
</head>

<script>
    function guardar() {
        let date = new Date();

        var name = jQuery("#username").val();
        var email = jQuery("#email").val();
        var password = jQuery("#password").val();
        var confirmerdPass = jQuery("#confirmPass").val()

        var params = {
            "name": name,
            "email": email,
            "password": password
        };
        console.log(params)

        if (comprobarContraseña(name, password, confirmerdPass) === true) {
            alert("hola")
            $.ajax({
                data: params,
                type: 'POST',
                url: '/register/addNewUser.php',
                success: function (response) {
                    console.log(response)
                    window.location.href = "../login/login.php"
                }

            });
        }

    }

    // function cargarLogin(){
    //     console.log("entra")
    //     $(document).ready(function (){
    //         $(".click").click(function (){
    //             console.log("carga")
    //             $("#crear").load("../login/login.php")
    //         })
    //     })
    // }

    function comprobarContraseña(name, password, confirmerdPass){

        if (name !== '' && password !== '' && confirmerdPass !== '' ){
            if (password === confirmerdPass){
                if (password.length >= 5 && strpbrk(password, "!#$.,:;()1234567890") !== false){
                    return true;
                }else{
                    alert("Tu contraseña no es lo suficientemente fuerte. Por favor utiliza otra")
                }
            }else{
                alert("Las contraseñas no coinciden.")
            }
        }else {
            alert("Por favor introduce los campos obligatorios.")
        }
    }

    function strpbrk(string, chars) {
        for(var i = 0, len = string.length; i < len; ++i) {
            if(chars.indexOf(string.charAt(i)) >= 0)
                return string.substring(i);
        }

        return false;
    }
</script>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700&display=swap');
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Poppins',sans-serif;
    }
    body{
        height: 100vh;
        display: block;
        justify-content: center;
        align-items: center;
        padding: 10px;
        background: linear-gradient(135deg, #71b7e6, #9b59b6);
    }
    .container{
        max-width: 700px;
        width: 100%;
        background-color: #fff;
        padding: 25px 30px;
        border-radius: 5px;
        box-shadow: 0 5px 10px rgba(0,0,0,0.15);
    }
    .container .title{
        font-size: 25px;
        font-weight: 500;
        position: relative;
    }
    .container .title::before{
        content: "";
        position: absolute;
        left: 0;
        bottom: 0;
        height: 3px;
        width: 30px;
        border-radius: 5px;
        background: linear-gradient(135deg, #71b7e6, #9b59b6);
    }
    .content form .user-details{
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        margin: 20px 0 12px 0;
    }
    form .user-details .input-box{
        margin-bottom: 15px;
        width: calc(100% / 2 - 20px);
    }
    form .input-box span.details{
        display: block;
        font-weight: 500;
        margin-bottom: 5px;
    }
    .user-details .input-box input{
        height: 45px;
        width: 100%;
        outline: none;
        font-size: 16px;
        border-radius: 5px;
        padding-left: 15px;
        border: 1px solid #ccc;
        border-bottom-width: 2px;
        transition: all 0.3s ease;
    }
    .user-details .input-box input:focus,
    .user-details .input-box input:valid{
        border-color: #9b59b6;
    }
    form .gender-details .gender-title{
        font-size: 20px;
        font-weight: 500;
    }
    form .category{
        display: flex;
        width: 80%;
        margin: 14px 0 ;
        justify-content: space-between;
    }
    form .category label{
        display: flex;
        align-items: center;
        cursor: pointer;
    }
    form .category label .dot{
        height: 18px;
        width: 18px;
        border-radius: 50%;
        margin-right: 10px;
        background: #d9d9d9;
        border: 5px solid transparent;
        transition: all 0.3s ease;
    }
    #dot-1:checked ~ .category label .one,
    #dot-2:checked ~ .category label .two,
    #dot-3:checked ~ .category label .three{
        background: #9b59b6;
        border-color: #d9d9d9;
    }
    form input[type="radio"]{
        display: none;
    }
    form .button{
        height: 45px;
        margin: 35px 0
    }
    form .button input{
        height: 100%;
        width: 100%;
        border-radius: 5px;
        border: none;
        color: #fff;
        font-size: 18px;
        font-weight: 500;
        letter-spacing: 1px;
        cursor: pointer;
        transition: all 0.3s ease;
        background: linear-gradient(135deg, #71b7e6, #9b59b6);
    }
    form .button input:hover{
        /* transform: scale(0.99); */
        background: linear-gradient(-135deg, #71b7e6, #9b59b6);
    }
    @media(max-width: 584px){
        .container{
            max-width: 100%;
        }
        form .user-details .input-box{
            margin-bottom: 15px;
            width: 100%;
        }
        form .category{
            width: 100%;
        }
        .content form .user-details{
            max-height: 300px;
            overflow-y: scroll;
        }
        .user-details::-webkit-scrollbar{
            width: 5px;
        }
    }
    @media(max-width: 459px){
        .container .content .category{
            flex-direction: column;
        }
    }

</style>

<body>
<h1>member system tutorial - register</h1>
<a href="../index.php">Home</a> |
<a href="./register.php">Register</a> |
<a href="../login/login.php">Login</a>
<hr />

<!-- error message code here -->

<!-- registration form html code here -->

</body>
</html>


<form action="./register.php" class="form" >

    <h1>Crear nueva cuenta</h1>

    <div class="">
<!--        --><?php
//        // check to see if the user successfully created an account
//        if (isset($success) && $success == true){
//            echo '<p color="green">Yay!! Your account has been created. <a href="../login/login.php">Click here</a> to login!<p>';
//        }
//        // check to see if the error message is set, if so display it
//        else if (isset($error_msg))
//            echo '<p color="red">'.$error_msg.'</p>';
//        ?>
    </div>

    <div class="">
        <input type="text" id="username" name="username" value="" placeholder="Usuario" autocomplete="off" required />
    </div>
    <div class="">
        <input type="text" id="email" name="email" value="" placeholder="Correo" autocomplete="off" required />
    </div>
    <div class="">
        <input type="password" id="password" name="passwd" value="" placeholder="Contraseña" autocomplete="off" required />
    </div>
    <div class="">
        <p>password must be at least 5 characters and<br /> have a special character, e.g. !#$.,:;()</font></p>
    </div>
    <div class="">
        <input type="password" id="confirmPass" name="confirm_password" value="" placeholder="confirm your password" autocomplete="off" required />
    </div>

    <div class="">
        <button  id="crear" onclick="guardar()">Crear Cuenta</button>
    </div>

    <p class="center"><br />
        Already have an account? <a href="../login/login.php">Login here</a>
    </p>
</form>

<?php
//if (isset($success) && $success == true){
//    echo '<font color="green">Yay!! Your account has been created. <a href="../login/login.php">Click here</a> to login!<font>';
//}
//// check to see if the error message is set, if so display it
//else if (isset($error_msg))
//    echo '<font color="red">'.$error_msg.'</font>';
//else
//    echo ''; // do nothing
//
//?>
