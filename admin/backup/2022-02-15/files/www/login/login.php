<?php
// login.php
//require_once ('../connection/bddconnection.php');
session_start();
if( isset($_POST['btn'])){
// Configura los datos de tu cuenta
    $dbhost='mysql-toysandshare.alwaysdata.net';  ## El servidor
    $dbusername='257522_toys'; ## Usuario
    $dbuserpass='toysandshare1'; ## Aqui va la clave de tu servidor de base de datos.
    $dbname='toysandshare_db';  ## Aqui va el nombre de la base de datos que te especifique arriba.

// Conectar a la base de datos
    $conn = mysqli_connect ($dbhost, $dbusername, $dbuserpass, $dbname);
//    mysqli_select_db($dbname) or die('Cannot select database');

## Escapamos los datos recibidos.
    $email = htmlentities(trim($_POST['email']));
    $password = htmlentities(trim($_POST['password'])); ## Es preferible que guarde tus password encriptados.

    ##Verificamos que el usuario contenga datos.
    if (isset($email) && empty($email) ) {
        echo "El usuario no fue instroducido";
        exit();
    }

    ## Verificamos que el password contiene password.
    if ( isset($password) && empty($password) ) {
        echo "El password no fue instroducido";
        exit();
    }

    $select = "SELECT email,password FROM users WHERE email = '$email' and password='$password'";

    ## Hacemos la consulta de verificacion.
    $query = mysqli_query($conn,$select) or die(mysqli_error());

    ## Verificamos si regreso registro la consulta.
    if(mysqli_num_rows($query)){
        ## recuperamos los datos.
        $data = mysqli_fetch_array($query);

        ## Guadamos el usuario en la seccion.
        $_SESSION["s_email"] = $data['email'];

        ## Mostramos el mensaje.
        header("Location: /index.php");
        echo "Has sido logueado correctamente ".$_SESSION['s_email']." y puedes acceder al index.php.";
        exit();
    } else {
        ##Mandamos ala mierda el que esta intentando entrar sin permisos.
        echo "Login incorrecto";

    }
}

?>

<style>

    * {
        padding: 0;
        box-sizing: border-box;
    }

    body {
        margin: 50px auto;
        text-align: center;
        width: 800px;
    }

    h1 {
        font-family: 'Passion One', serif;
        font-size: 2rem;
        text-transform: uppercase;
    }

    label {
        width: 150px;
        font-family: 'Passion One ', serif;
        display: inline-flex;
        text-align: left;
        font-size: 1.5rem;
    }

    input {
        border-radius: 0.5em;
        border: 2px solid #000000;
        font-size: 1.5rem;
        font-weight: 100;
        font-family: 'Lato', serif;
        padding: 10px;
    }

    form {
        margin: 25px auto;
        padding: 20px;
        border: 5px solid #000000;
        width: 500px;
        background: #7ed9c8;
    }

    div.form-element {
        margin: 20px 0;
    }

    button {
        padding: 10px;
        font-size: 2vw;
        font-family: 'Passion One', serif;
        font-weight: 100;
        background: #f23545;
        color: #000000;
        border: 2px solid #000000;
    }

    p.success,
    p.error {
        color: white;
        font-family: lato, "Lato Black", serif;

        background: #055159;
        display: inline-block;
        padding: 2px 10px;
    }

    p.error {
        background: orangered;
    }

</style>
<!DOCTYPE html>
<html>
<head>
    <title>Login page</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>Log in</h1>
<?php //if ($submitted && !$authenticated): ?>
<!--    <div class="alert">-->
<!--        Invalid credentials.-->
<!--    </div>-->
<?php //endif; ?>
<?php //if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
//    //Valid email!
//
//} ?>
<form method="POST" id="acceso" action="./login.php" name="signin-form">
    <h2>Build a better World together</h2>
    <p>Sharing & Caring isn’t that difficult</p>
    <div class="form-element">
        <label>Email</label>
        <input id="email" type="text" class="acceso" name="email" placeholder="Email" required />
    </div>
    <div class="form-element">
        <label>Password</label>
        <input id="password" type="password" class="acceso" name="password" required />
    </div>
    <button type="submit" name="login-google" value="login-google">Log in with Google</button>
    <button type="submit" name="btn" value="login" >Log In</button>
</form>
</body>
</html>
